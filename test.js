var P={"0":
[{"id": 10,
  "title": "House",
  "level": 0,
  "children": [],
  "parent_id": null}],
"1":
[{"id": 12,
  "title": "Red Roof",
  "level": 1,
  "children": [],
  "parent_id": 10},
 {"id": 18,
  "title": "Blue Roof",
  "level": 1,
  "children": [],
  "parent_id": 10},
 {"id": 13,
  "title": "Wall",
  "level": 1,
  "children": [],
  "parent_id": 10}],
"2":
[{"id": 17,
  "title": "Blue Window",
  "level": 2,
  "children": [],
  "parent_id": 12},
 {"id": 16,
  "title": "Door",
  "level": 2,
  "children": [],
  "parent_id": 13},
 {"id": 15,
  "title": "Red Window",
  "level": 2,
  "children": [],
  "parent_id": 12}]}
  var arry = [];
  

for(var i in P)
{
    for(var j in P[i])
    {
        arry.push( P [i][j]);
    }
   
} 
function treeify(nodes) {
    var indexed_nodes = {}, tree_roots = [];
    for (var i = 0; i < nodes.length; i += 1) {
        indexed_nodes[nodes[i].id] = nodes[i];
    }
    for (var i = 0; i < nodes.length; i += 1) {
        var parent_id = nodes[i].parent_id;
        if (parent_id === null) {
            tree_roots.push(nodes[i]);
        } else {
            indexed_nodes[parent_id].children.push(nodes[i]);
        }
    }
    return tree_roots;
}

console.log(JSON.stringify(treeify(arry), undefined, "\t"));

